package com.learningbydoing;

import java.io.*;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;

import com.aspose.pdf.HtmlLoadOptions;
import com.aspose.pdf.License;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class HttpsUrlConnectionForSAPSF {

    private List<String> cookies;
    private HttpsURLConnection conn;

    private final String USER_AGENT = "Mozilla/5.0";
    private static int count = 0;

    public static void main(String[] args) throws Exception {

        String url = "https://pmsalesdemo8.successfactors.com/login";
//        String landingPage = "https://pmsalesdemo8.successfactors.com/acme?bplte_company=SFPART017772&fbacme_o=my_profile&madr_selected_user=108736&fbsp__CompPerSt&fbscps_per_st_type=COMP_PERSONAL_STATEMENT&fbscps_selected_per_st=581%5eSimple%20personal%20compensation%20statement%5e108736&fbscps_form_temp_id=null&fbscps_program_id=-1&_s.crb=FDqIoucGYTztWocm71%2bkpBfcTQU%3d";
        String landingPage = "https://pmsalesdemo8.successfactors.com/";
        HttpsUrlConnectionForSAPSF http = new HttpsUrlConnectionForSAPSF();

        // make sure cookies is turn on
        CookieHandler.setDefault(new CookieManager());

        // 1. Send a "GET" request, so that you can extract the form's data.
        String page = http.GetPageContent(url);
        String postParams = http.getFormParams(page, "sfadmin", "Cloud@1234");

        // 2. Construct above post's content and then send a POST request for
        // authentication
        http.sendPost(url, postParams);
        count++;
        // 3. success then go to gmail.
        String result = http.GetPageContent(landingPage);
        System.out.println(result);
    }

    private void sendPost(String url, String postParams) throws Exception {

        URL obj = new URL(url);
        conn = (HttpsURLConnection) obj.openConnection();

        // Acts like a browser
        conn.setUseCaches(false);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Host", "pmsalesdemo8.successfactors.com");
        conn.setRequestProperty("User-Agent", USER_AGENT);
        conn.setRequestProperty("Accept",
                "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        if (cookies != null) {
            for (String cookie : this.cookies) {
                conn.addRequestProperty("Cookie", cookie.split(";", 1)[0]);
            }
        }
        conn.setRequestProperty("Connection", "keep-alive");
        conn.setRequestProperty("Referer", "https://pmsalesdemo8.successfactors.com/login");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", Integer.toString(postParams.length()));

        conn.setDoOutput(true);
        conn.setDoInput(true);

        // Send post request
        DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
        wr.writeBytes(postParams);
        wr.flush();
        wr.close();

        int responseCode = conn.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + postParams);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in =
                new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

    }

    private String GetPageContent(String url) throws Exception {

        URL obj = new URL(url);
        conn = (HttpsURLConnection) obj.openConnection();

        // default is GET
        conn.setRequestMethod("GET");

        conn.setUseCaches(false);

        // act like a browser
        conn.setRequestProperty("User-Agent", USER_AGENT);
        conn.setRequestProperty("Accept",
                "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        if (cookies != null) {
            for (String cookie : this.cookies) {
                conn.addRequestProperty("Cookie", cookie.split(";", 1)[0]);
            }
        }
        int responseCode = conn.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        if(count == 1) {
            new License().setLicense(
                    new FileInputStream(new File("D:\\CCIDC\\Workspace\\Aspose.Pdf.lic")));
            HtmlLoadOptions options = new HtmlLoadOptions("https://pmsalesdemo8.successfactors.com/");
            com.aspose.pdf.Document document = new com.aspose.pdf.Document(conn.getInputStream(), options);
            document.save("D:\\CCIDC\\Workspace\\jars\\" + "WebPageToPDF_SAPSF_HOME.pdf");
        }
        BufferedReader in =
                new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        // Get the response cookies
        setCookies(conn.getHeaderFields().get("Set-Cookie"));

        return response.toString();

    }

    public String getFormParams(String html, String username, String password)
            throws UnsupportedEncodingException {

        System.out.println("Extracting form's data...");

        Document doc = Jsoup.parse(html);

        Elements inputElements = doc.getElementsByTag("input");
        List<String> paramList = new ArrayList<String>();
        paramList.add("company=" + URLEncoder.encode("SFPART017772", "UTF-8"));
        int i = 0;
        for (Element inputElement : inputElements) {
            String key = inputElement.attr("name");
            String value = inputElement.attr("value");
            if(i == 2)
                break;
            if (key.equals("username"))
                value = username;
            else if (key.equals("password"))
                value = password;
            else
                continue;
            paramList.add(key + "=" + URLEncoder.encode(value, "UTF-8"));
            i++;
        }

        // build parameters list
        StringBuilder result = new StringBuilder();
        for (String param : paramList) {
            if (result.length() == 0) {
                result.append(param);
            } else {
                result.append("&" + param);
            }
        }
        return result.toString();
    }

    public List<String> getCookies() {
        return cookies;
    }

    public void setCookies(List<String> cookies) {
        this.cookies = cookies;
    }

}
