package com.learningbydoing;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TableHeadWidthRemover {
    private static final Pattern tableHeadWidthPattern;
    private static final Pattern tableDataDivDivStyleWidthPattern;

    static {
        tableHeadWidthPattern = Pattern.compile("<th.*\\s*>");
        tableDataDivDivStyleWidthPattern = Pattern.compile("<td.*\\s*>\\s*<div.*><div.*>");
    }

    public static String removeTableHeadWidth(String content) {
        Matcher m = tableHeadWidthPattern.matcher(content);

        Map<String, String> toRep = new LinkedHashMap<>();
        while (m.find()) {
            toRep.put(m.group(), m.group().replaceAll("width=\".*\"", " "));
        }

        String replaced = content;
        for (Iterator<Map.Entry<String, String>> itr = toRep.entrySet().iterator(); itr.hasNext(); ) {
            Map.Entry<String, String> entry = itr.next();
            String key = entry.getKey();
            String val = entry.getValue();
            replaced = replaced.replaceAll(key, val);
        }
        return replaced;
    }

    public static String removeTableHeadWidthAndTableDataDivDivStyleWidth(String content) {
        String replaced = removeTableHeadWidth(content);
        Matcher m = tableDataDivDivStyleWidthPattern.matcher(replaced);

        Map<String, String> toRep = new LinkedHashMap<>();
        while (m.find()) {
            toRep.put(m.group(), m.group().replaceAll("width:100%", " "));
        }

        for (Iterator<Map.Entry<String, String>> itr = toRep.entrySet().iterator(); itr.hasNext(); ) {
            Map.Entry<String, String> entry = itr.next();
            String key = entry.getKey();
            String val = entry.getValue();
            replaced = replaced.replaceAll(key, val);
        }
        return replaced;
    }
}
