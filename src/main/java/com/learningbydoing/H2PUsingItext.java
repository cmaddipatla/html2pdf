package com.learningbydoing;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.html2pdf.css.media.MediaDeviceDescription;
import com.itextpdf.html2pdf.css.media.MediaType;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;

import java.io.*;
import java.net.URL;

public class H2PUsingItext {
    public static final String HTML = "<h1>Test</h1><p>Hello World</p>";
    public static final String BASEURI = "D:\\CCIDC\\Workspace\\jars\\itext\\";
    public static final String SRC = String.format("%shello.html", BASEURI);
    public static final String TARGET = "D:\\CCIDC\\Workspace\\jars\\itext\\";
    public static final String DEST = String.format("%stest-01.pdf", TARGET);

    public static void main(String[] args) throws Exception {
        File file = new File(TARGET);
        file.mkdirs();
//        new H2PUsingItext().createPdf(HTML, DEST);
        FileInputStream inputStream = new FileInputStream("D:\\CCIDC\\Workspace\\jars\\fromapp.txt");
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while ((i = inputStream.read()) != -1) {  // to reach until the laste bytecode -1
            sb.append((char) i);
        }
        new H2PUsingItext().createPdf(new URL("https://en.wikipedia.org/wiki/Help:References_and_page_numbers"), DEST);
    }

//    public void createPdf(String html, String dest) throws IOException {
//        HtmlConverter.convertToPdf(html, new FileOutputStream(dest));
//    }

    public void createPdf(URL url, String dest) throws IOException {
        PdfWriter writer = new PdfWriter(dest);
        PdfDocument pdf = new PdfDocument(writer);
        PageSize pageSize = new PageSize(850, 1700);
        pdf.setDefaultPageSize(pageSize);
        ConverterProperties properties = new ConverterProperties();
        MediaDeviceDescription mediaDeviceDescription =
                new MediaDeviceDescription(MediaType.SCREEN);
        mediaDeviceDescription.setWidth(pageSize.getWidth());
        properties.setMediaDeviceDescription(mediaDeviceDescription);
        HtmlConverter.convertToPdf(url.openStream(), pdf, properties);
    }
}
