package com.learningbydoing;

import org.zefer.pd4ml.PD4Constants;
import org.zefer.pd4ml.PD4ML;

import java.awt.*;
import java.io.FileOutputStream;

public class PD4MLTest {
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        try (FileOutputStream outStream = new FileOutputStream("D:\\CCIDC\\Workspace\\jars\\store\\okay.pdf")) {
            PD4ML pd4ml = new PD4ML();
            pd4ml.setPageInsets(new Insets(20, 10, 10, 10));
            pd4ml.setHtmlWidth(1000);
            pd4ml.setPageSize(PD4Constants.A4);
            pd4ml.enableDebugInfo();
            pd4ml.render("file:" + "D:\\CCIDC\\Workspace\\jars\\store\\Objectives-without-dom-manipulations.html", outStream);
            System.out.println("Total Time: " + (System.currentTimeMillis() - startTime) + " milliseconds.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
