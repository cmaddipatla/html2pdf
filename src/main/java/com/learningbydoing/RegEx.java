package com.learningbydoing;

import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegEx {
    public static void main(String[] args) throws Exception {
        {
            FileInputStream inputStream = new FileInputStream("D:\\CCIDC\\Workspace\\jars\\Objectives-without-dom-manipulations.html");
            StringBuilder sb = new StringBuilder();
            int content;
            while ((content = inputStream.read()) != -1) {
                sb.append((char) content);
            }
            String string = sb.toString();
            String replaced = TableHeadWidthRemover.removeTableHeadWidthAndTableDataDivDivStyleWidth(string);

            Files.write(Paths.get("D:\\CCIDC\\Workspace\\jars\\Objectives-without-dom-manipulations-test.html"), replaced.getBytes());
        }
    }
    public static void removeTableHeadWidth(String[] args) throws Exception {
        FileInputStream inputStream = new FileInputStream("D:\\CCIDC\\Workspace\\jars\\Objectives-without-dom-manipulations.html");
        StringBuilder sb = new StringBuilder();
        int content;
        while ((content = inputStream.read()) != -1) {
            sb.append((char) content);
        }
        String string = sb.toString();
        Pattern pattern = Pattern.compile("<th.*\\s*>");
        Matcher m = pattern.matcher(string);

        boolean b = m.matches();


        Map<String, String> toRep = new LinkedHashMap<>();
        while (m.find()) {
            toRep.put(m.group(), m.group().replaceAll("width=\".*\"", " "));
        }

        String replaced = string;

        for(Iterator<Map.Entry<String, String>> itr = toRep.entrySet().iterator(); itr.hasNext();) {
            Map.Entry<String, String> entry = itr.next();
            String key = entry.getKey();
            String val = entry.getValue();
            replaced = replaced.replaceAll(key, val);
        }


        System.out.println(replaced);

//        System.out.println(string);
//        string = string.replaceAll("(?:\\s*)width\\s*(=\\s*([\"'])[^\"']+\\2\\s*|:\\s*[^;]+;)"," ");
        Files.write(Paths.get("D:\\CCIDC\\Workspace\\jars\\Objectives-without-dom-manipulations-test.html"), replaced.getBytes());
    }

}
