package com.learningbydoing;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.net.ssl.*;
import java.io.*;
import java.net.Socket;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.cert.CertificateException;

public class ParseLinksUsingJSOUP {
    public static void certificateByPass() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509ExtendedTrustManager() {
                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }

                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                        }

                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] xcs, String string, Socket socket) throws CertificateException {

                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] xcs, String string, Socket socket) throws CertificateException {

                        }

                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] xcs, String string, SSLEngine ssle) throws CertificateException {

                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] xcs, String string, SSLEngine ssle) throws CertificateException {

                        }

                    }
            };

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        certificateByPass();
        Document doc;
        String baseUrl = "https://pmsalesdemo8-successfactors-com.qa.ciphercloud.in";
        try {
            doc = Jsoup.parse(new File("D:\\CCIDC\\Workspace\\jars\\store\\Objectives-without-dom-manipulations.html"), "UTF-8");
            // get all links
            Elements links = doc.select("a[href]");
            Elements css = doc.select("link[href]");
            links.addAll(css);
            storeInFile(baseUrl, links, "href");

            Elements images = doc.select("img[src]");
            Elements scripts = doc.select("script[src]");
            images.addAll(scripts);
            storeInFile(baseUrl, scripts, "src");
            FileOutputStream outputStream = new FileOutputStream("D:\\CCIDC\\Workspace\\jars\\store\\Objectives-without-dom-manipulations.html");
            outputStream.write(doc.toString().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        long endTime = System.currentTimeMillis();
        System.err.println("Time taken in milliseconds: " + (endTime - startTime));
    }

    private static void storeInFile(String baseUrl, Elements links, String key) throws IOException {
        String fileName;
        for (Element link : links) {
            // get the value from href attribute
            String relativeUrl = link.attr(key);
            link.attr(key, "." + relativeUrl);
            String[] splits = relativeUrl.split("/");
            fileName = splits[splits.length - 1];
            if (!(fileName.endsWith(".js") || fileName.endsWith(".gif") || fileName.endsWith(".css"))) {
                continue;
            }
            URL obj = new URL(baseUrl + relativeUrl);
            HttpsURLConnection conn = (HttpsURLConnection) obj.openConnection();
            conn.setUseCaches(false);
            conn.setHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            BufferedReader in =
                    new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            String completePath = "D:\\CCIDC\\Workspace\\jars\\store\\" + relativeUrl.substring(0, relativeUrl.lastIndexOf("/"));
            Path path = Paths.get(completePath);
            if (!Files.exists(path)) {
                try {
                    Files.createDirectories(path);
                } catch (IOException e) {
                    //fail to create directory
                    e.printStackTrace();
                }
            }
            FileOutputStream outputStream = new FileOutputStream(completePath + "/" + fileName);
            outputStream.write(response.toString().getBytes());
        }
    }
}