package com.learningbydoing;

import com.aspose.pdf.Document;
import com.aspose.pdf.HtmlLoadOptions;
import com.aspose.pdf.License;
import com.aspose.pdf.PageInfo;

import java.io.FileInputStream;

public class Http {

    public static void main(String[] args) throws Exception {
        new License().setLicense(Http.class.getClassLoader().getResourceAsStream("Aspose.Pdf.lic"));

        HtmlLoadOptions options = new HtmlLoadOptions("https://pmsalesdemo8.successfactors.com");
        PageInfo info = new PageInfo();
        info.setLandscape(true);
        options.setPageInfo(info);
        Document document = new Document(new FileInputStream("C:\\Users\\cmaddipatla\\Desktop\\a.html"), options);
        document.save("C:\\Users\\cmaddipatla\\Desktop\\pdf.pdf");
    }
}
